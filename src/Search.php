<?php

declare(strict_types=1);

namespace Amneale\TorrentSearch;

use Amneale\TorrentSearch\Provider\Provider;

class Search
{
    /**
     * @var Provider[]
     */
    private array $providers;

    /**
     * @param Provider ...$providers
     */
    public function __construct(Provider ...$providers)
    {
        $this->providers = $providers;
    }

    /**
     * @return TorrentResult[]
     */
    public function search(string $query): array
    {
        if (empty($this->providers)) {
            throw new \RuntimeException('No search providers have been configured');
        }

        $torrents = [];

        foreach ($this->providers as $provider) {
            $torrents[] = $provider->search($query);
        }

        $torrents = array_merge(...$torrents);
        $torrents = array_unique($torrents, SORT_REGULAR);

        usort($torrents, static function (TorrentResult $a, TorrentResult $b) {
            return $b->seeds === $a->seeds
                ? $b->peers <=> $a->peers
                : $b->seeds <=> $a->seeds;
        });

        return $torrents;
    }
}
