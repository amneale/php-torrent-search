<?php

declare(strict_types=1);

namespace Amneale\TorrentSearch\Provider;

use Amneale\ByteForm\ByteParser;
use Amneale\Torrent\Magnet;
use Amneale\TorrentSearch\TorrentResult;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Symfony\Component\DomCrawler\Crawler;

class PirateBayProvider extends HttpProvider
{
    private const SIZE_REGEX = '/(\d*\.?\d+)(?:\s*)([BKMGT]+(?:i?B?))/uix';

    private ByteParser $byteParser;

    public function __construct(
        string $baseUrl,
        ClientInterface $httpClient,
        RequestFactoryInterface $requestFactory,
        ByteParser $byteParser
    ) {
        $this->byteParser = $byteParser;

        parent::__construct($baseUrl, $httpClient, $requestFactory);
    }

    protected function buildUri(string $baseUrl, string $query): string
    {
        return sprintf('%s/search/%s/0/99/0', $baseUrl, $query);
    }

    protected function parseResponseString(string $responseAsString): array
    {
        $crawler = new Crawler($responseAsString);

        $torrentResults = $crawler->filter('table#searchResult')
            ->children('tr')
            ->each(function (Crawler $node) {
                $row = $node->children('td');
                $info = $row->eq(1);

                if (0 === $info->count()) {
                    return null;
                }

                $link = $info->children('a');

                if (0 === $link->count()) {
                    return null;
                }

                $magnetUri = $link->first()->attr('href');
                $torrent = Magnet::fromUri($magnetUri)->toTorrent();
                $desc = $info->filter('font.detDesc');

                if (1 === $desc->count()) {
                    preg_match(self::SIZE_REGEX, $desc->text(), $matches);
                    $byteString = str_replace('i', '', $matches[1] . $matches[2]);
                    $torrent->size = $this->byteParser->parseBytes($byteString);
                }

                return new TorrentResult($torrent, (int) $row->eq(2)->text(), (int) $row->eq(3)->text());
            });

        return array_filter($torrentResults);
    }
}
