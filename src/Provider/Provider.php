<?php

declare(strict_types=1);

namespace Amneale\TorrentSearch\Provider;

use Amneale\TorrentSearch\TorrentResult;

interface Provider
{
    /**
     * @return TorrentResult[]
     */
    public function search(string $query): array;
}
