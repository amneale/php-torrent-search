<?php

declare(strict_types=1);

namespace Amneale\TorrentSearch\Provider;

use Amneale\TorrentSearch\TorrentResult;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;

abstract class HttpProvider implements Provider
{
    private string $baseUrl;
    private ClientInterface $httpClient;
    private RequestFactoryInterface $requestFactory;

    public function __construct(
        string $baseUrl,
        ClientInterface $httpClient,
        RequestFactoryInterface $requestFactory
    ) {
        $this->baseUrl = $baseUrl;
        $this->httpClient = $httpClient;
        $this->requestFactory = $requestFactory;
    }

    /**
     * @inheritDoc
     *
     * @throws ClientExceptionInterface
     */
    public function search(string $query): array
    {
        if (empty($query)) {
            throw new \InvalidArgumentException('Query can not be empty');
        }

        $uri = $this->buildUri($this->baseUrl, $query);
        $request = $this->requestFactory->createRequest('GET', $uri);
        $response = $this->httpClient->sendRequest($request);

        if (200 !== $response->getStatusCode()) {
            throw new \RuntimeException('Search returned non-200 status: ' . $response->getStatusCode());
        }

        $responseAsString = (string) $response->getBody();

        if (empty($responseAsString)) {
            throw new \RuntimeException('Search response was empty');
        }

        return $this->parseResponseString($responseAsString);
    }

    abstract protected function buildUri(string $baseUrl, string $query): string;

    /**
     * @return TorrentResult[]
     */
    abstract protected function parseResponseString(string $responseAsString): array;
}
