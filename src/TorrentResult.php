<?php

declare(strict_types=1);

namespace Amneale\TorrentSearch;

use Amneale\Torrent\Torrent;

final class TorrentResult
{
    public Torrent $torrent;
    public int $seeds;
    public int $peers;

    public function __construct(Torrent $torrent, int $seeds = 0, int $peers = 0)
    {
        $this->torrent = $torrent;
        $this->seeds = $seeds;
        $this->peers = $peers;
    }
}
