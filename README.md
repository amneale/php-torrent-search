# php-torrent-search ![tests](https://github.com/amneale/php-torrent-search/workflows/tests/badge.svg)

## Installation
```bash
$ composer require amneale/php-torrent-search
```

## Contributing
### Running tests
index-rate-api-sdk-php uses [phpspec](https://www.phpspec.net) to drive development

```bash
$ make test
```

### Checking code style
Code style rules are defined in `.php_cs.dist`. These rules are used to automatically fix any code style discrepancies

```bash
$ make code-style