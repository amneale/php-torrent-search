<?php

declare(strict_types=1);

namespace spec\Amneale\TorrentSearch;

use Amneale\Torrent\Torrent;
use Amneale\TorrentSearch\Provider\Provider;
use Amneale\TorrentSearch\TorrentResult;
use PhpSpec\ObjectBehavior;

class SearchSpec extends ObjectBehavior
{
    private const QUERY = 'test';

    public function it_excepts_if_no_providers_are_configured(): void
    {
        $this->shouldThrow(\RuntimeException::class)->during('search', [self::QUERY]);
    }

    public function it_returns_unique_results(Provider $providerA, Provider $providerB): void
    {
        $this->beConstructedWith($providerA, $providerB);

        $resultA = new TorrentResult(new Torrent('a'));
        $resultB = new TorrentResult(new Torrent('b'));
        $resultX = new TorrentResult(new Torrent('x'));

        $providerA->search(self::QUERY)->shouldBeCalled()->willReturn([$resultA, $resultX]);
        $providerB->search(self::QUERY)->shouldBeCalled()->willReturn([$resultB, $resultX]);

        $this->search(self::QUERY)->shouldReturn([$resultA, $resultX, $resultB]);
    }

    public function it_sorts_results_by_seeds_and_peers(Provider $provider): void
    {
        $this->beConstructedWith($provider);

        $resultA = new TorrentResult(new Torrent('a'), 10, 25);
        $resultB = new TorrentResult(new Torrent('b'), 10, 50);
        $resultC = new TorrentResult(new Torrent('c'), 50, 50);
        $resultD = new TorrentResult(new Torrent('d'), 20, 20);

        $provider->search(self::QUERY)
            ->shouldBeCalled()
            ->willReturn([$resultA, $resultB, $resultC, $resultD]);

        $this->search(self::QUERY)->shouldReturn([$resultC, $resultD, $resultB, $resultA]);
    }
}
