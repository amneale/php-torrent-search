<?php

declare(strict_types=1);

namespace spec\Amneale\TorrentSearch\Provider;

use Amneale\ByteForm\ByteParser;
use Amneale\Torrent\Torrent;
use Amneale\TorrentSearch\Provider\Provider;
use Amneale\TorrentSearch\TorrentResult;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class PirateBayProviderSpec extends ObjectBehavior
{
    public function let(
        ClientInterface $httpClient,
        RequestFactoryInterface $requestFactory,
        ByteParser $byteParser,
        RequestInterface $request,
        ResponseInterface $response,
        StreamInterface $stream
    ): void {
        $this->beConstructedWith('foo.bar.baz', $httpClient, $requestFactory, $byteParser);

        $requestFactory->createRequest('GET', Argument::containingString('foo.bar.baz'))->willReturn($request);
        $httpClient->sendRequest($request)->willReturn($response);
        $response->getBody()->willReturn($stream);
    }

    public function it_is_a_provider(): void
    {
        $this->shouldImplement(Provider::class);
    }

    public function it_searches(ResponseInterface $response, StreamInterface $stream, ByteParser $byteParser): void
    {
        $response->getStatusCode()->willReturn(200);
        $stream->__toString()->shouldBeCalled()->willReturn(
            $this->getResponseContent()
        );
        $byteParser->parseBytes(Argument::any())->willReturn(1024);

        $results = $this->search('test');
        $results->shouldBeArray();
        $results->shouldHaveCount(4);

        $results[0]->shouldBeLike(
            new TorrentResult(
                new Torrent(
                    '4e1243bd22c66e76c2ba9eddc1f91394e57f9f83',
                    'Test Torrent',
                    [
                        'http://foo.bar.tracker:80',
                        'udp://foo.bar.tracker:1234',
                    ],
                    1024
                ),
                500,
                150,
            )
        );

        $results[1]->shouldBeLike(
            new TorrentResult(
                new Torrent(
                    '830ec2475d53a3b5ca656f465753f4b09905cd25',
                    'My Test Torrent',
                    [
                        'http://foo.bar.tracker:80',
                        'udp://foo.bar.tracker:1234',
                    ],
                    1024
                ),
                300,
                123
            )
        );

        $results[2]->shouldBeLike(
            new TorrentResult(
                new Torrent(
                    '89633302168d03b7288fd81a4434eb395cb2ad21',
                    'Another Test Torrent',
                    [
                        'http://foo.bar.tracker:80',
                        'udp://foo.bar.tracker:1234',
                    ],
                    1024
                ),
                100,
                38
            )
        );

        $results[3]->shouldBeLike(
            new TorrentResult(
                new Torrent(
                    '9d9778c314da0db9eaff812e4977ff2cb961d9fa',
                    'Test Torrent Book',
                    [
                        'http://foo.bar.tracker:80',
                        'udp://foo.bar.tracker:1234',
                    ],
                    1024
                ),
                20,
                12
            )
        );
    }

    public function it_excepts_if_no_query_is_provided(): void
    {
        $this->shouldThrow(\InvalidArgumentException::class)->during('search', ['']);
    }

    public function it_excepts_for_non_200_status(ResponseInterface $response): void
    {
        $response->getStatusCode()->willReturn(500);
        $this->shouldThrow(\RuntimeException::class)->during('search', ['test']);
    }

    public function it_excepts_for_empty_body(ResponseInterface $response, StreamInterface $stream): void
    {
        $response->getStatusCode()->willReturn(200);
        $stream->__toString()->shouldBeCalled()->willReturn('');
        $this->shouldThrow(\RuntimeException::class)->during('search', ['test']);
    }

    private function getResponseContent(): string
    {
        return <<<EOT
<!DOCTYPE html>
<html lang="en">
<body>
<div id="SearchResults">
    <table id="searchResult">
        <thead id="tableHead">
            <tr class="header">
                <th><a href="/search/test/0/13/0" title="Order by Type">Type</a></th>
                <th><div class="sortby"><a href="/search/tests/0/1/0" title="Order by Name">Name</a></div></th>
                <th><abbr title="Seeders"><a href="/search/test/0/8/0" title="Order by Seeders">SE</a></abbr></th>
                <th><abbr title="Leechers"><a href="/search/test/0/9/0" title="Order by Leechers">LE</a></abbr></th>
            </tr>
        </thead>
        <tr>
            <td class="vertTh">
                <center>
                    <a href="/browse/200" title="More from this category">Video</a><br />
                    (<a href="/browse/207" title="More from this category">Movies</a>)
                </center>
            </td>
            <td>
                <div class="detName">
                    <a href="/torrent/12345/Test Torrent" class="detLink" title="Details for Test Torrent">Test Torrent</a>
                </div>
                <a href="magnet:?xt=urn:btih:4e1243bd22c66e76c2ba9eddc1f91394e57f9f83&dn=Test+Torrent&tr=http%3A%2F%2Ffoo.bar.tracker%3A80&tr=udp%3A%2F%2Ffoo.bar.tracker%3A1234" title="Download this torrent using magnet"><img src="/static/img/icon-magnet.gif" alt="Magnet link" /></a>
                <font class="detDesc">Uploaded 07-12&nbsp;2012, Size 3.7&nbsp;GiB, ULed by <a class="detDesc" href="/user/test/" title="Browse test">test</a></font>
            </td>
            <td align="right">500</td>
            <td align="right">150</td>
        </tr>
        <tr class="alt">
            <td class="vertTh">
                <center>
                    <a href="/browse/200" title="More from this category">Video</a><br />
                    (<a href="/browse/207" title="More from this category">Movies</a>)
                </center>
            </td>
            <td>
                <div class="detName">
                    <a href="/torrent/12346/My Test Torrent" class="detLink" title="Details for My Test Torrent">My Test Torrent</a>
                </div>
                <a href="magnet:?xt=urn:btih:830ec2475d53a3b5ca656f465753f4b09905cd25&dn=My+Test+Torrent&tr=http%3A%2F%2Ffoo.bar.tracker%3A80&tr=udp%3A%2F%2Ffoo.bar.tracker%3A1234" title="Download this torrent using magnet"><img src="/static/img/icon-magnet.gif" alt="Magnet link" /></a>
                <font class="detDesc">Uploaded 11-05&nbsp;2013, Size 1.2&nbsp;GiB, ULed by <a class="detDesc" href="/user/test/" title="Browse test">test</a></font>
            </td>
            <td align="right">300</td>
            <td align="right">123</td>
        </tr>
        <tr>
            <td class="vertTh">
                <center>
                    <a href="/browse/200" title="More from this category">Video</a><br />
                    (<a href="/browse/207" title="More from this category">Movies</a>)
                </center>
            </td>
            <td>
                <div class="detName">
                    <a href="/torrent/12347/Another Test Torrent" class="detLink" title="Details for Another Test Torrent">Another Test Torrent</a>
                </div>
                <a href="magnet:?xt=urn:btih:89633302168d03b7288fd81a4434eb395cb2ad21&dn=Another+Test+Torrent&tr=http%3A%2F%2Ffoo.bar.tracker%3A80&tr=udp%3A%2F%2Ffoo.bar.tracker%3A1234" title="Download this torrent using magnet"><img src="/static/img/icon-magnet.gif" alt="Magnet link" /></a>
                <font class="detDesc">Uploaded 02-11&nbsp;2015, Size 2.5&nbsp;GiB, ULed by <a class="detDesc" href="/user/test/" title="Browse test">test</a></font>
            </td>
            <td align="right">100</td>
            <td align="right">38</td>
        </tr>
        <tr class="alt">
            <td class="vertTh">
                <center>
                    <a href="/browse/201" title="More from this category">Books</a><br />
                    (<a href="/browse/208" title="More from this category">Non-Fiction</a>)
                </center>
            </td>
            <td>
                <div class="detName">
                    <a href="/torrent/12348/Test Torrent Book" class="detLink" title="Test Torrent Book">Test Torrent Book</a>
                </div>
                <a href="magnet:?xt=urn:btih:9d9778c314da0db9eaff812e4977ff2cb961d9fa&dn=Test+Torrent+Book&tr=http%3A%2F%2Ffoo.bar.tracker%3A80&tr=udp%3A%2F%2Ffoo.bar.tracker%3A1234" title="Download this torrent using magnet"><img src="/static/img/icon-magnet.gif" alt="Magnet link" /></a>
                <font class="detDesc">Uploaded 03-03&nbsp;2012, Size 31&nbsp;MiB, ULed by <a class="detDesc" href="/user/test/" title="Browse test">test</a></font>
            </td>
            <td align="right">20</td>
            <td align="right">12</td>
        </tr>
    </table>
</div>
</body>
</html>
EOT;
    }
}
